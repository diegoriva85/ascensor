# ascensor
instalacion:
Descargar el proyecto
ejecutar: composer install para installar las dependencias de laravel
ejecutar: npm install para dependencias de Vue

Crear una base mysql con
 CREATE DATABASE `ascensor` /*!40100 DEFAULT CHARACTER SET utf8 */;

Definición de archivos:
/.env se encuentra la configuracion de acceso a la base local
ej: 
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=ascensor
	DB_USERNAME=root
	DB_PASSWORD=

 
Estos comando crean y populan las tablas
php artisan migrate:install
php artisan migrate
composer dump-autoload
php artisan db:seed



Ejemplo de ejecucion:
php artisan serve para correr el servidor de laravel
npm run watch para compilar Vue 

usuario de login 
email = test@test.com
password = qwerty123