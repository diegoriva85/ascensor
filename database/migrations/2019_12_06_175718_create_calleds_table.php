<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalledsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calleds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('elevator_number')->unsigned();
            $table->integer('frequency')->unsigned();
            $table->integer('flors')->unsigned()->nullable()->default(0);
            $table->integer('start')->unsigned()->nullable()->default(0);
            $table->integer('end')->unsigned()->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calleds');
    }
}
