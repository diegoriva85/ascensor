<?php

use Illuminate\Database\Seeder;
use App\Elevator;
class ElevatorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            ['number' => 1],
            ['number' => 2],
            ['number' => 3]);

        Elevator::insert($data);
    }
}
