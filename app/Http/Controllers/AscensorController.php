<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Called;
use App\Elevator;

class Ascensorcontroller extends Controller
{
    /**
     * Se busca desde la base todas las llamas programadas y se iteran para carcular el resultado deseado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $called = Called::all();
        $result['calls']=$called;
        $result['registers'] = array();
        foreach ($called as $key => $value) {
            for ($t=$value->start; $t < $value->end; $t= $t +$value->frequency) { 
                if (strlen($value->flors)>1){
                    $texto = strval($value->flors);
                    for ($c=0; $c <strlen($texto); $c++) { 
                        $flors = intval($texto[$c]);
                        $called[$value->elevator_number]['cant_flors'] += $flors + $flors;
                        $line = array('number' => $value->elevator_number ,
                                'position' =>  $flors ,
                                'flors' => $flors + $flors,
                                'cant_flors' => $called[$value->elevator_number]['cant_flors']);
                        }
                }else {
                    $sum_row = $value->flors + $value->flors;
                    $called[$value->elevator_number]['cant_flors'] += $sum_row;  
                    
                    $line = array('number' => $value->elevator_number ,
                            'position' => $value->flors ,
                            'flors' => $sum_row,
                            'cant_flors' => $called[$value->elevator_number]['cant_flors']);
                }
                array_push($result['registers'],$line);
            
            }
    }

    return $result;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $call = new Called();
        $call->elevator_number = $request->elevator_number;
        $call->frequency = $request->frequency;
        $call->flors = $request->flors;
        $call->start = ($request->start *60 );
        $call->end =  ($request->end *60);

        $call->save();
       return Ascensorcontroller::index();
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $call = Called::find($id);
        $call->elevator_number = $request->elevator_number;
        $call->frequency = $request->frequency;
        $call->flors = $request->flors;
        $call->start = ($request->start *60 );
        $call->end =  ($request->end *60 );
        
        $call->save();
       return Ascensorcontroller::index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $call = Called::find($id);
        $call->delete();
        return Ascensorcontroller::index();
    }
}
